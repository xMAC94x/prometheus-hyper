[![Crates.io](https://img.shields.io/crates/v/prometheus-hyper.svg?label=prometheus-hyper)](https://crates.io/crates/prometheus-hyper)
[![docs.rs](https://docs.rs/prometheus-hyper/badge.svg)](https://docs.rs/prometheus-hyper)
[![pipeline status](https://gitlab.com/xMAC94x/prometheus-hyper/badges/master/pipeline.svg)](https://gitlab.com/xMAC94x/prometheus-hyper/commits/master)
[![coverage report](https://gitlab.com/xMAC94x/prometheus-hyper/badges/master/coverage.svg)](https://gitlab.com/xMAC94x/prometheus-hyper/commits/master)
[![license](https://img.shields.io/crates/l/prometheus-hyper)](https://gitlab.com/xMAC94x/prometheus-hyper/blob/master/LICENSE-MIT)
[![dependencies](https://deps.rs/repo/gitlab/xMAC94x/prometheus-hyper/status.svg)](https://deps.rs/repo/gitlab/xMAC94x/prometheus-hyper)
[![lines of code](https://tokei.rs/b1/gitlab/xMAC94x/prometheus-hyper)](https://tokei.rs/b1/gitlab/xMAC94x/prometheus-hyper)

# prometheus-hyper

Helper library to export prometheus metrics using [tokio](https://crates.io/crates/tokio) and [hyper](https://crates.io/crates/hyper).
It's intended to help writing prometheus exporters without the need to setup and maintain a http (no https) webserver.
If the program also uses a http server for other purposes this package is probably not the best way and
[prometheus](https://crates.io/crates/prometheus) should be used directly.

This crate is similar to [prometheus_exporter](https://crates.io/crates/prometheus_exporter).
If you are not in a `tokio` environment you might choose that one.

Use this crate when:
 - you write an async app that wants to export metrics
 - want to interact directly with [prometheus](https://crates.io/crates/prometheus).
 - don't want to care about the webserver to host the `/metrics` endpoint

Don't use this crate when:
 - you write a rest-api, in that case you should prob just implement the `/metrics` endpoint with your rest-framework.
 - you want to avoid `tokio`.
 - you are writing a library, in that case use plain [prometheus](https://crates.io/crates/prometheus) and let the binary application decide which HTTP server to use.

This crate optimizes for typical metrics scraping, e.g. a scrape interval by few prometheus instances (usually 1) with a usual interval (e.g. 1s).
It's optimized for a low foot-print, rather than being able to serve 100.000 metrics requests per sec.

## Usage

Add this to your `Cargo.toml`:

```toml
[dependencies]
prometheus-hyper = "0.2"
```

The crate exports a single struct `Server` with a single fn `run`:
Pass your registry to the server, and provide it with a shutdown_future.
The server will shut down, once the future completes.

```rust
tokio::spawn(Server::run(
    Arc::clone(&registry),
    SocketAddr::from(([0; 4], 8080)),
    std::future::pending(),
));
```

This will start the exporter and bind the http server to `0.0.0.0:8080`. After
that you can just update the metrics as you used to.

See the [documentation](https://docs.rs/prometheus-hyper) and the [examples](/examples) for more information on how to use this crate.
